-- Telescope -- fuzzy finder
return {'nvim-telescope/telescope.nvim',
  version = '0.1.x',
  dependencies = {
    { 'nvim-lua/plenary.nvim' },
    { 'BurntSushi/ripgrep' }, -- for grep function
    { -- fuzzy finder (more efficient searching)
      'nvim-telescope/telescope-fzf-native.nvim',
      build = 'make',
    }
  },
  keys = {
    {"<leader>ft", ":Telescope<CR>", desc = "Telescope builtins"},
    {"<leader>ff", ":Telescope find_files<CR>", desc = "Telescope find files"},
    {"<leader>fg", ":Telescope live_grep<CR>", desc = "Telescope live grep"},
    {"<leader>fr", ":Telescope oldfiles<CR>", desc = "Telescope recent files"},
    {"<leader>fh", ":Telescope help_tags<CR>", desc = "Telescope help tags"},
    {"<leader>fc", ":Telescope commands<CR>", desc = "Telescope list commands"},
  },
  opts = {
    extensions = {
      fzf = {
        fuzzy = true,
        override_generic_sorter = true,
        override_file_sorter = true,
        case_mode = 'smart_case',
      },
    },
  },
  config = function(opts)
    require('telescope'):setup(opts)
  end
}
