-- Treesitter -- syntax highlighting
return {
  "nvim-treesitter/nvim-treesitter",
  event = "VeryLazy",
  build = ":TSUpdate",
  config = function()
    local configs = require("nvim-treesitter.configs")
    configs.setup({
      ensure_installed = {
        "lua",
        "bash",
        --"python",
        --"markdown",
        --"javascript",
        --"typescript",
        --"html",
        --"css",
        --"scss",
        --"c",
        --"rust",
      },
      sync_install = true,
      auto_install = true,
      highlight = {
        enable = true
      },
    })
  end
}
