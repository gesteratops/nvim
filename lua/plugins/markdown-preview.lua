-- Markdown-Preview - Live preview
return {
  "iamcco/markdown-preview.nvim",
  ft = "markdown",
  keys = {
    {"<leader>mm", ":MarkdownPreview<CR>", desc = "Markdown Preview"},
  },
  build = {
    function()
      vim.fn["mkdp#util#install"]()
    end
  }
}
