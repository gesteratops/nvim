return {
  "rose-pine/neovim",
  name = "rose-pine",
  lazy = false,
  config = function()
    require("rose-pine").setup({
      dark_variant = "main",
      styles = {
        italic = false
      }
    })
    vim.o.background = "dark"
    vim.cmd("colorscheme rose-pine-main")
  end
}
