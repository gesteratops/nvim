-- Oil.nvim - file explorer to edit filesystem like a normal nvim buffer
return {
  "stevearc/oil.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  opts = {
    default_file_explorer = false,
  },
  keys = {
    {"-", ":Oil<CR>", desc = "Open parent directory" }
  },
  config = function ()
    require("oil").setup()
  end
}
