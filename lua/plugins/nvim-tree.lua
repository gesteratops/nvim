-- Nvim-Tree file explorer
return  {
  "kyazdani42/nvim-tree.lua",
  dependencies = {
    "nvim-tree/nvim-web-devicons"
  },
  keys = {
    {"<leader>tt", ":NvimTreeToggle<CR>", desc = "Nvim-tree toggle"},
    {"<leader>to", ":NvimTreeOpen<CR>", desc = "Nvim-tree open"},
    {"<leader>tq", ":NvimTreeClose<CR>", desc = "Nvim-tree close"},
  },
  opts = {
    filters = {
      dotfiles = false
    },
    view = {
      adaptive_size = true,
      width = 50,
      side = "right",
      preserve_window_proportions = true,
      number = true,
      relativenumber = true,
    },
    actions = {
      open_file = {
        quit_on_open = true,
        resize_window = true,
      }
    }
  },
}
