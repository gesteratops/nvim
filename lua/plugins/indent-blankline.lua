-- Indent-Blankline -- indent guidelines
return {
  "lukas-reineke/indent-blankline.nvim",
  event = "VeryLazy",
  main = "ibl",
  opts = {
    enabled = true,
    scope = { enabled = false },
    exclude = {
      filetypes = {
        "help",
        "dashboard",
        "lazy",
      },
    },
  },
}
